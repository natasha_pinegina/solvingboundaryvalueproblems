#pragma once

#include <cmath>
#include <vector>
#include "SolvingBoundaryValueProblemsDlg.h"

using namespace std;

//��������� ��������� �������
struct Entry�onditions
{
	// ���������� ����� �������. ����. 500
	int size;
	// ����� ��������� �������. ����. 5 �����
	double left;
	// ������ ��������� �������. ����. 0 �����
	double right;
	// ������� ��������. ����. 2 20 200 ���
	double length;
	// ������������ ����������� �������. ����. 10-100
	double nA;
	// ������� ������������. ����. 1e15
	double n0;
	// �������� �������. ����. 1e-8
	double eps;
};


class Plate
{
private:
	int size;
	double left;
	double right;
	double length;
	
	double nA;
	double n0;

	double step;
	double eps;


	bool is_field;

	vector <double> curr_fi;
	vector <double> next_fi;
	vector <double> keys;

	vector <double> a;
	vector <double> b;

	vector <double> psi;

public:
	Plate(Entry�onditions& problem_desc)
	{
		size = problem_desc.size;
		left = problem_desc.left;
		right = problem_desc.right;

		nA = problem_desc.nA;
		n0 = problem_desc.n0;

		double q = -1.6e-19;
		double k = 1.38064852e-23;
		double T = 300;
		double epsSi = 12;
		double epsVac = 8.86e-12;
		//����� �����
		double Ld = sqrt((epsSi * epsVac * k * T) / (q * q * n0 * (/*nD +*/ nA)));


		length = problem_desc.length * 1e-6 / Ld;

		step = length / size;
		eps = problem_desc.eps;

		curr_fi.resize(size + 1);
		next_fi.resize(size + 1);
		keys.resize(size + 1);
		a.resize(size + 1);
		b.resize(size + 1);
		psi.resize(size + 1);

		for (int i = 0; i <= size; i++)
		{
			keys[i] = length * i / size;
		}

		for (int i = 0; i <= size; i++)
		{
			next_fi[i] = left + (right - left) * i / size;
		}
	}
	void solve(vector<double>& solve_buff, vector<double>& keys_buff, vector<double>& Solve, int& iter)
	{
		iter = 0;
		double curr_eps = 0;
		do
		{
			curr_fi = next_fi;
			forward();
			backward();
			curr_eps = count_difference();
			iter++;
			for (int i = 0; i < curr_fi.size(); i++)
			{
				Solve.push_back(curr_fi[i]);
			}
			if (iter > 100) break;
		} while (curr_eps > eps && curr_eps!=0);
		solve_buff = next_fi;
		keys_buff = keys;
	}
private:
	
	double F(double pot)
	{
		return -(nA * (1.0 - exp(pot)));
	}


	double q(double pot)
	{
		return -(nA * exp(pot));
	}

	double r(double pot)
	{
		return  F(pot) + q(pot) * pot;
	}


	// ������ ���
	void forward()
	{
		//��������� �������
		a[0] = 0;
		b[0] = left;

		for (int i = 1; i <= size; i++)
		{
			a[i] = a[i - 1] + (a[i - 1] * a[i - 1] * q(curr_fi[i - 1]) + 1) * step;
			b[i] = b[i - 1] + (a[i - 1] * b[i - 1] * q(curr_fi[i - 1]) - a[i - 1] * r(curr_fi[i - 1])) * step;
		}

		next_fi[size] = right;
		psi[size] = (next_fi[size] - b[size]) / a[size];
	}
	// �������� ���
	void backward()
	{
		for (int i = size - 1; i >= 0; i--)
		{
			psi[i] = psi[i + 1] - (r(curr_fi[i + 1]) - q(curr_fi[i + 1]) * (a[i + 1] * psi[i + 1] + b[i + 1])) * step;
		}

		for (int i = 0; i <= size; i++)
		{
			next_fi[i] = a[i] * psi[i] + b[i];
		}
	}

	double count_difference()
	{
		double summ = 0;
		double der = 0;
		for (int i = 0; i <= size; i++)
		{
			summ += (next_fi[i] - curr_fi[i]) * (next_fi[i] - curr_fi[i]);
			der += next_fi[i] * next_fi[i];
		}

		return summ;
	}
};
