﻿
// SolvingBoundaryValueProblemsDlg.h: файл заголовка
//

#pragma once
#include <vector>

// Диалоговое окно CSolvingBoundaryValueProblemsDlg
class CSolvingBoundaryValueProblemsDlg : public CDialogEx
{
// Создание
public:
	CSolvingBoundaryValueProblemsDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SOLVINGBOUNDARYVALUEPROBLEMS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV

	CWnd* PicWnd_V;
	CDC* PicDc_V;
	CRect Pic_V;


	CPen koordpen, netkoordpen, signalpen, spectrpen, vsignalpen;
	CPen* pen;
	CFont fontgraph;
	CFont* font;

	CPen osi_pen;		// для осей 
	CPen setka_pen;		// для сетки
	CPen graf_pen;		// для графика функции
	CPen graf_pen2;
	CPen graf_pen3;


	double xp, yp,			//коэфициенты пересчета
		xmin, xmax,			//максисимальное и минимальное значение х 
		ymin, ymax;			//максисимальное и минимальное значение y

	double  xx0, xxmax, yy0, yymax, xxi, yyi, iter;
	char
		znach[1000];

	void Draw1Graph(std::vector<double>&, CDC*, CRect, CPen*, int, int, CString, CString);



// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double Na;
	double V;
	double L;
	double Eps;
	afx_msg void OnBnClickedButton1();
	CButton U;
	CButton VAH;
	afx_msg void nx(std::vector<double>&);
	std::vector<double> Nx;
	double Q;
	std::vector<double> Container;
	double v;
};
