﻿
// SolvingBoundaryValueProblemsDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "SolvingBoundaryValueProblems.h"
#include "SolvingBoundaryValueProblemsDlg.h"
#include "afxdialogex.h"
#include "Plate.h"
#include <cmath>
#include <vector>
#include <cstdlib>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CSolvingBoundaryValueProblemsDlg



CSolvingBoundaryValueProblemsDlg::CSolvingBoundaryValueProblemsDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SOLVINGBOUNDARYVALUEPROBLEMS_DIALOG, pParent)
	, Na(50)
	, V(5)
	, L(20)
	, Eps(1e-28)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSolvingBoundaryValueProblemsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, Na);
	DDX_Text(pDX, IDC_EDIT2, V);
	DDX_Text(pDX, IDC_EDIT3, L);
	DDX_Text(pDX, IDC_EDIT4, Eps);
	DDX_Control(pDX, IDC_U, U);
	DDX_Control(pDX, IDC_VAH, VAH);
}

BEGIN_MESSAGE_MAP(CSolvingBoundaryValueProblemsDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CSolvingBoundaryValueProblemsDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// Обработчики сообщений CSolvingBoundaryValueProblemsDlg

BOOL CSolvingBoundaryValueProblemsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	PicWnd_V = GetDlgItem(IDC_V);
	PicDc_V = PicWnd_V->GetDC();
	PicWnd_V->GetClientRect(&Pic_V);


	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		0.1,						//толщина 1 пиксель
		RGB(0, 0, 250));		//цвет  черный
	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 0, 0));			//цвет черный

	graf_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет черный
	graf_pen2.CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	graf_pen3.CreatePen(PS_SOLID, 2, RGB(155, 150, 150));

	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_U));
	pcb1->SetCheck(1);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CSolvingBoundaryValueProblemsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CSolvingBoundaryValueProblemsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CSolvingBoundaryValueProblemsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CSolvingBoundaryValueProblemsDlg::Draw1Graph(std::vector<double>& Mass, CDC* WinDc, CRect WinPic, CPen* graphpen, int KolToch, int AbsMax, CString Abs, CString Ord)
{
	// поиск максимального и минимального значения
	xmin = Mass[0];
	xmax = Mass[0];
	for (int i = 0; i < Mass.size(); i++)
	{
		if (Mass[i] < xmin)
		{
			xmin = Mass[i];
		}
		if (Mass[i] > xmax)
		{
			xmax = Mass[i];
		}
	}

	//---- отрисовка -------------------------------------------------
	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(
		WinDc,
		WinPic.Width(),
		WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	//------ заливка фона графика белым цветом ---------------------------------
	MemDc->FillSolidRect(
		WinPic,
		RGB(
			255,
			255,
			255));
	//------ отрисовка сетки координат -----------------------------------------
	pen = MemDc->SelectObject(&setka_pen);
	// вертикальные линии сетки координат
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width(); i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, 0);
		MemDc->LineTo(i, WinPic.Height());
	}
	// горизонтальные линии сетки координат
	for (float i = (float)WinPic.Height() / 10; i < WinPic.Height(); i += (float)WinPic.Height() / 10)
	{
		MemDc->MoveTo(0, i);
		MemDc->LineTo(WinPic.Width(), i);
	}
	//------ отрисовка осей ----------------------------------------------------
	pen = MemDc->SelectObject(&osi_pen);
	// отрисовка оси X
	MemDc->MoveTo(2, (float)WinPic.Height() * 9 / 10);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 + 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	MemDc->MoveTo(WinPic.Width() - 15, (float)WinPic.Height() * 9 / 10 - 2);
	MemDc->LineTo(WinPic.Width() - 2, (float)WinPic.Height() * 9 / 10);
	// деления на оси X
	for (float i = (float)WinPic.Width() / 25; i < WinPic.Width() * 24 / 25; i += (float)WinPic.Width() / 25)
	{
		MemDc->MoveTo(i, WinPic.Height() * 9 / 10 + 2);
		MemDc->LineTo(i, WinPic.Height() * 9 / 10 - 3);
	}
	//// отрисовка оси Y
	if (VAH.GetCheck())
	{
		MemDc->MoveTo(WinPic.Width() / 2 + 15, WinPic.Height() - 2);
		MemDc->LineTo(WinPic.Width() / 2 + 15, 2);
		MemDc->MoveTo(WinPic.Width() / 2 + 15-2, 15);
		MemDc->LineTo(WinPic.Width() / 2 + 15, 2);
		MemDc->MoveTo(WinPic.Width() / 2 + 15+2, 15);
		MemDc->LineTo(WinPic.Width() / 2 + 15, 2);
	}
	if (U.GetCheck())
	{
		MemDc->MoveTo(WinPic.Width() * 2 / 25, WinPic.Height() - 2);
		MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
		MemDc->MoveTo(WinPic.Width() * 2 / 25 - 2, 15);
		MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
		MemDc->MoveTo(WinPic.Width() * 2 / 25 + 2, 15);
		MemDc->LineTo(WinPic.Width() * 2 / 25, 2);
	}
	//------ подписи осей --------------------------------------------
	// установка прозрачного фона текста
	MemDc->SetBkMode(TRANSPARENT);
	// установка шрифта
	MemDc->SelectObject(&fontgraph);
	// подпись оси X
	MemDc->TextOut((float)WinPic.Width() * 24 / 25 + 4, (float)WinPic.Height() * 9 / 10 + 2, Abs);
	// подпись оси Y
	MemDc->TextOut((float)WinPic.Width() * 2 / 25 + 5, 0, Ord);
	// выбор области для рисования
	xx0 = WinPic.Width() * 2 / 25;
	xxmax = WinPic.Width() * 24 / 25;
	yy0 = WinPic.Height() / 10;
	yymax = WinPic.Height() * 9 / 10;
	// отрисовка
	pen = MemDc->SelectObject(&graf_pen);
	MemDc->MoveTo(xx0, yymax + (Mass[0] - xmin) / (xmax - xmin) * (yy0 - yymax));
	for (int i = 0; i < Mass.size(); i++)
	{
		xxi = xx0 + (xxmax - xx0) * i / (Mass.size() - 1);
		yyi = yymax + (Mass[i] - xmin) / (xmax - xmin) * (yy0 - yymax);
		//MemDc->MoveTo(xx0, yymax + (Mass[0] - xmin) / (xmax - xmin) * (yy0 - yymax));
		MemDc->LineTo(xxi, yyi);
	}
	//for (int i = 6; i < 25; i += 5)
	//{
	//	sprintf(znach, "%5.1f", (i - 1) * (float)AbsMax / 22);
	//	MemDc->TextOut(i * WinPic.Width() / 25 + 2, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//}
	//// по оси ординат
	//sprintf(znach, "%5.2f", xmax);
	//MemDc->TextOut(0, WinPic.Height() / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.75 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 5 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.5 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 9 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.25 * xmax);
	//MemDc->TextOut(0, WinPic.Height() * 13 / 20 + 1, CString(znach));
	//sprintf(znach, "%5.2f", 0.0);
	//MemDc->TextOutW(0, WinPic.Height() * 9 / 10 + 2, CString(znach));
	//----- вывод на экран -------------------------------------------
	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CSolvingBoundaryValueProblemsDlg::nx(std::vector<double>& fi)
{
	Nx.clear();
	double n = 0;
	double q = -1.6e-19;
	double k = 1.38064852e-23;
	double T = 300;
	Q = 0;
	for (int i = 0; i < fi.size(); i++)
	{
		if(v >0)
		n = -Na * exp(q*fi[i]/(k*T));
		if(v<0)
		n = -Na * exp(-q * fi[i] / (k * T));
		Nx.push_back(n);
		Q += n;
	}
}

void CSolvingBoundaryValueProblemsDlg::OnBnClickedButton1()
{
	UpdateData(true);
	int size = 200;
	if (U.GetCheck())
	{
		EntryСonditions con = { size,V,0,L,Na,1e15,Eps };
		Plate plate(con);
		vector<double> solve_buff;
		vector<double> keys_buff;
		vector<double> Solve;
		int iter = 0;
		plate.solve(solve_buff, keys_buff, Solve, iter);
		Draw1Graph(solve_buff, PicDc_V, Pic_V, &graf_pen3, size, size, CString("x"), CString("Fi"));
	}
	if (VAH.GetCheck())
	{
		Container.clear();
		//V = 0.1;
		double C = 0;
		vector<double> solve_buff;
		vector<double> keys_buff;
		vector<double> Solve;
		for (v= -3; v <0.01 ; v+=0.1)
		{
			solve_buff.clear();
			keys_buff.clear();
			Solve.clear();
			EntryСonditions con = {size,v,0,L,Na,1e15,Eps};
			Plate plate(con);
			int iter = 0;
			plate.solve(solve_buff, keys_buff, Solve, iter);
			nx(solve_buff);
			C = Q / v;
			Container.push_back(C);
		}
		for (v = 0.01; v < 3; v += 0.1)
		{
			solve_buff.clear();
			keys_buff.clear();
			Solve.clear();
			EntryСonditions con = { size,v,0,L,Na,1e15,Eps };
			Plate plate(con);
			int iter = 0;
			plate.solve(solve_buff, keys_buff, Solve, iter);
			nx(solve_buff);
			C = Q / v;
			Container.push_back(C);
		}
		Draw1Graph(Container, PicDc_V, Pic_V, &graf_pen3, size, size, CString("U"), CString("С"));
	}	
}
