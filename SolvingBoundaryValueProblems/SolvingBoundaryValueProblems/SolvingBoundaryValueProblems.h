﻿
// SolvingBoundaryValueProblems.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CSolvingBoundaryValueProblemsApp:
// Сведения о реализации этого класса: SolvingBoundaryValueProblems.cpp
//

class CSolvingBoundaryValueProblemsApp : public CWinApp
{
public:
	CSolvingBoundaryValueProblemsApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CSolvingBoundaryValueProblemsApp theApp;
